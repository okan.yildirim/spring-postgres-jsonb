package com.okan.springpostgresjsonb.domain.util;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


class StringDecoratorTest {

    @Test
    public void it_should_wrap_with_double_quote() {
        //given

        //when
        String wrapped = StringDecorator.wrapDoubleQuote("BLACK");
        //then
        assertThat(wrapped).isEqualTo("\"BLACK\"");
    }
}