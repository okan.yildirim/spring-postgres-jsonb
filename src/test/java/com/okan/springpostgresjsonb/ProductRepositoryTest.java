package com.okan.springpostgresjsonb;

import com.okan.springpostgresjsonb.domain.Color;
import com.okan.springpostgresjsonb.domain.FinanceInfo;
import com.okan.springpostgresjsonb.domain.Photo;
import com.okan.springpostgresjsonb.domain.Product;
import com.okan.springpostgresjsonb.domain.util.StringDecorator;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ProductRepositoryTest extends AbstractPostgresContainer {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    TestEntityManager testEntityManager;

    @Test
    public void it_should_find_product_by_id() {
        //given
        Product product = new Product();
        product.setPrice(10D);
        product.setName("product");

        product.addSellerId(111L);
        product.addSellerId(222L);

        product.addColors(Color.BLUE);
        product.addColors(Color.GREEN);

        product.addPhotos(new Photo(10, 50, "path1"));
        product.addPhotos(new Photo(100, 500, "path2"));

        product.setFinanceInfo(new FinanceInfo(10, 20));

        Product savedProduct = productRepository.save(product);

        testEntityManager.flush();
        testEntityManager.clear();
        //when
        Optional<Product> fetchedProduct = productRepository.findById(savedProduct.getId());
        //then
        assertThat(fetchedProduct).isPresent();
        assertThat(fetchedProduct.get().getName()).isEqualTo("product");
    }

    @Test
    public void it_should_find_product_by_seller_id() {
        //given
        Product product = new Product();
        product.setPrice(10D);
        product.setName("product");

        product.addSellerId(111L);
        product.addSellerId(222L);

        product.addColors(Color.BLUE);
        product.addColors(Color.GREEN);

        product.addPhotos(new Photo(10, 50, "path1"));
        product.addPhotos(new Photo(100, 500, "path2"));

        product.setFinanceInfo(new FinanceInfo(10, 20));

        productRepository.save(product);

        testEntityManager.flush();
        testEntityManager.clear();
        //when
        List<Product> products = productRepository.findBySellerId("111");
        //then
        assertThat(products.get(0).getName()).isEqualTo("product");
    }

    @Test
    public void it_should_find_product_add_seller_id_to_product() {
        //given
        Product product = new Product();
        product.setPrice(10D);
        product.setName("product");

        product.addSellerId(111L);
        product.addSellerId(222L);

        product.addColors(Color.BLUE);
        product.addColors(Color.GREEN);

        product.addPhotos(new Photo(10, 50, "path1"));
        product.addPhotos(new Photo(100, 500, "path2"));

        product.setFinanceInfo(new FinanceInfo(10, 20));

        Product savedProduct = productRepository.save(product);

        testEntityManager.flush();
        testEntityManager.clear();
        //when
        int isSuccessful = productRepository.addSellerIdToProduct(savedProduct.getId(), "333");

        testEntityManager.flush();
        testEntityManager.clear();

        Optional<Product> updatedProduct = productRepository.findById(savedProduct.getId());
        //then
        assertThat(isSuccessful).isEqualTo(1);
        assertThat(updatedProduct.get().getName()).isEqualTo("product");
        assertThat(updatedProduct.get().getSellerIds().contains(111L)).isTrue();
        assertThat(updatedProduct.get().getSellerIds().contains(222L)).isTrue();
        assertThat(updatedProduct.get().getSellerIds().contains(333L)).isTrue();
    }

    @Test
    @Disabled // TODO
    public void it_should_remove_seller_id_from_product() {
        //given
        Product product = new Product();
        product.setPrice(10D);
        product.setName("product");

        product.addSellerId(111L);
        product.addSellerId(222L);

        product.addColors(Color.BLUE);
        product.addColors(Color.GREEN);

        product.addPhotos(new Photo(10, 50, "path1"));
        product.addPhotos(new Photo(100, 500, "path2"));

        product.setFinanceInfo(new FinanceInfo(10, 20));

        Product savedProduct = productRepository.save(product);

        testEntityManager.flush();
        testEntityManager.clear();
        //when
        int isSuccessful = productRepository.removeSellerIdFromProduct(savedProduct.getId(), 222L);

        testEntityManager.flush();
        testEntityManager.clear();

        Optional<Product> updatedProduct = productRepository.findById(savedProduct.getId());
        //then
        assertThat(isSuccessful).isEqualTo(1);
        assertThat(updatedProduct.get().getName()).isEqualTo("product");
        assertThat(updatedProduct.get().getSellerIds().contains(111L)).isTrue();
        assertThat(updatedProduct.get().getSellerIds().contains(222L)).isFalse();
    }

    @Test
    public void it_should_return_zero_when_product_is_not_updated() {
        //given
        Product product = new Product();
        product.setPrice(10D);
        product.setName("product");

        product.addSellerId(111L);
        product.addSellerId(222L);

        product.addColors(Color.BLUE);
        product.addColors(Color.GREEN);

        product.addPhotos(new Photo(10, 50, "path1"));
        product.addPhotos(new Photo(100, 500, "path2"));

        product.setFinanceInfo(new FinanceInfo(10, 20));

        productRepository.save(product);

        testEntityManager.flush();
        testEntityManager.clear();
        //when
        int isSuccessful = productRepository.addSellerIdToProduct(3L, "333");

        testEntityManager.flush();
        testEntityManager.clear();

        //then
        assertThat(isSuccessful).isEqualTo(0);
    }

    @Test
    public void it_should_find_product_by_color() {
        //given
        Product product = new Product();
        product.setPrice(10D);
        product.setName("product");

        product.addSellerId(111L);
        product.addSellerId(222L);

        product.addColors(Color.BLUE);
        product.addColors(Color.GREEN);

        product.addPhotos(new Photo(10, 50, "path1"));
        product.addPhotos(new Photo(100, 500, "path2"));

        product.setFinanceInfo(new FinanceInfo(10, 20));

        productRepository.save(product);

        testEntityManager.flush();
        testEntityManager.clear();
        //when
        List<Product> products = productRepository.findByColor(StringDecorator.wrapDoubleQuote(Color.GREEN.name()));
        //then
        assertThat(products.get(0).getName()).isEqualTo("product");
    }

    @Test
    public void it_should_add_color_id_to_product() {
        //given
        Product product = new Product();
        product.setPrice(10D);
        product.setName("product");

        product.addSellerId(111L);
        product.addSellerId(222L);

        product.addColors(Color.BLUE);
        product.addColors(Color.GREEN);

        product.addPhotos(new Photo(10, 50, "path1"));
        product.addPhotos(new Photo(100, 500, "path2"));

        product.setFinanceInfo(new FinanceInfo(10, 20));

        Product savedProduct = productRepository.save(product);

        testEntityManager.flush();
        testEntityManager.clear();
        //when
        int isSuccessful = productRepository.addColorToProduct(
                savedProduct.getId(), StringDecorator.wrapDoubleQuote(Color.RED.getName()));

        testEntityManager.flush();
        testEntityManager.clear();

        Optional<Product> updatedProduct = productRepository.findById(savedProduct.getId());
        //then
        assertThat(isSuccessful).isEqualTo(1);
        assertThat(updatedProduct.get().getName()).isEqualTo("product");
        assertThat(updatedProduct.get().getColors().contains(Color.BLUE)).isTrue();
        assertThat(updatedProduct.get().getColors().contains(Color.GREEN)).isTrue();
        assertThat(updatedProduct.get().getColors().contains(Color.RED)).isTrue();
    }

    @Test
    public void it_should_remove_color_from_product() {
        //given
        Product product = new Product();
        product.setPrice(10D);
        product.setName("product");

        product.addSellerId(111L);
        product.addSellerId(222L);

        product.addColors(Color.BLUE);
        product.addColors(Color.GREEN);

        product.addPhotos(new Photo(10, 50, "path1"));
        product.addPhotos(new Photo(100, 500, "path2"));

        product.setFinanceInfo(new FinanceInfo(10, 20));

        Product savedProduct = productRepository.save(product);

        testEntityManager.flush();
        testEntityManager.clear();
        //when
        int isSuccessful = productRepository.removeColorFromProduct(
                savedProduct.getId(), Color.GREEN.getName());

        testEntityManager.flush();
        testEntityManager.clear();

        Optional<Product> updatedProduct = productRepository.findById(savedProduct.getId());
        //then
        assertThat(isSuccessful).isEqualTo(1);
        assertThat(updatedProduct.get().getName()).isEqualTo("product");
        assertThat(updatedProduct.get().getColors().contains(Color.BLUE)).isTrue();
        assertThat(updatedProduct.get().getColors().contains(Color.GREEN)).isFalse();
    }


    @Test
    public void it_should_find_product_by_finance_info() {
        //given
        Product product = new Product();
        product.setPrice(10D);
        product.setName("product");

        product.addSellerId(111L);
        product.addSellerId(222L);

        product.addColors(Color.BLUE);
        product.addColors(Color.GREEN);

        product.addPhotos(new Photo(10, 50, "path1"));
        product.addPhotos(new Photo(100, 500, "path2"));

        product.setFinanceInfo(new FinanceInfo(10, 20));

        productRepository.save(product);

        testEntityManager.flush();
        testEntityManager.clear();
        //when
        List<Product> products = productRepository.findByFinanceInfo(new FinanceInfo(10, 20));
        //then
        assertThat(products.get(0).getName()).isEqualTo("product");
    }

    @Test
    public void it_should_find_product_by_finance_commission_grater_than() {
        //given
        Product product1 = new Product();
        product1.setPrice(10D);
        product1.setName("product1");

        product1.addSellerId(111L);
        product1.addSellerId(222L);

        product1.addColors(Color.BLUE);
        product1.addColors(Color.GREEN);

        product1.addPhotos(new Photo(10, 50, "path1"));
        product1.addPhotos(new Photo(100, 500, "path2"));

        product1.setFinanceInfo(new FinanceInfo(10, 20));

        Product product2 = new Product();
        product2.setPrice(20D);
        product2.setName("product2");
        product2.setFinanceInfo(new FinanceInfo(5, 7));
        productRepository.save(product1);
        productRepository.save(product2);

        testEntityManager.flush();
        testEntityManager.clear();
        //when
        List<Product> products = productRepository.findCommissionGreaterThan(9D);
        //then
        assertThat(products.get(0).getName()).isEqualTo("product1");
    }

    @Test
    public void it_should_update_payment_term_of_product() {
        // given
        Product product = new Product();
        product.setPrice(10D);
        product.setName("product");

        product.addSellerId(111L);
        product.addSellerId(222L);

        product.addColors(Color.BLUE);
        product.addColors(Color.GREEN);

        product.addPhotos(new Photo(10, 50, "path1"));
        product.addPhotos(new Photo(100, 500, "path2"));

        product.setFinanceInfo(new FinanceInfo(10, 20));

        Product savedProduct = productRepository.save(product);

        testEntityManager.flush();
        testEntityManager.clear();
        //when
        int isSuccessful = productRepository.updatePaymentTerm(savedProduct.getId(), "10");

        testEntityManager.flush();
        testEntityManager.clear();

        Optional<Product> updatedProduct = productRepository.findById(savedProduct.getId()); //

        //then
        assertThat(isSuccessful).isEqualTo(1);
        assertThat(updatedProduct.get().getName()).isEqualTo("product");
        assertThat(updatedProduct.get().getFinanceInfo().getPaymentTerm()).isEqualTo(10);
    }
}