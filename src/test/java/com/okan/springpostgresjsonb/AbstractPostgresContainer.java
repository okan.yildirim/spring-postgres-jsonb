package com.okan.springpostgresjsonb;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;

@ContextConfiguration(initializers = AbstractPostgresContainer.Initializer.class)
public abstract class AbstractPostgresContainer {

    static final PostgreSQLContainer POSTGRE_SQL_CONTAINER;


    static {
        POSTGRE_SQL_CONTAINER = new PostgreSQLContainer("postgres:11.1-alpine")
                .withDatabaseName("jsonb-example")
                .withUsername("jsonb")
                .withPassword("jsonb");

        POSTGRE_SQL_CONTAINER.start();
    }

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of("spring.datasource.url=" + POSTGRE_SQL_CONTAINER.getJdbcUrl(),
                    "spring.datasource.username=" + POSTGRE_SQL_CONTAINER.getUsername(),
                    "spring.datasource.password=" + POSTGRE_SQL_CONTAINER.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
