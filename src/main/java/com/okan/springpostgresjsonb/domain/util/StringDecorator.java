package com.okan.springpostgresjsonb.domain.util;

public class StringDecorator {

    public static final String DOUBLE_QUOTE = "\"";

    public static String wrapDoubleQuote(String s) {
      return new StringBuilder().append(DOUBLE_QUOTE)
                .append(s)
                .append(DOUBLE_QUOTE)
                .toString();
    }
}
