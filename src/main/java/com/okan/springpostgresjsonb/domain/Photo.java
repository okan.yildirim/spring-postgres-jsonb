package com.okan.springpostgresjsonb.domain;

import java.io.Serializable;

public class Photo implements Serializable {

    private double width;
    private double height;
    private String path;

    public Photo(int width, int height, String path) {
        this.width = width;
        this.height = height;
        this.path = path;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
