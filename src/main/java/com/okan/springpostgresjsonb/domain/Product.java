package com.okan.springpostgresjsonb.domain;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "products")
@TypeDefs({@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)})
public class Product {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private Double price;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Set<Long> sellerIds = new HashSet<>();

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Set<Color> colors = new HashSet<>();

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private List<Photo> photos = new ArrayList<>();


    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private FinanceInfo financeInfo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Set<Long> getSellerIds() {
        return sellerIds;
    }

    public void addSellerId(Long sellerId) {
        this.sellerIds.add(sellerId);
    }

    public Set<Color> getColors() {
        return colors;
    }

    public void addColors(Color color) {
        this.colors.add(color);
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void addPhotos(Photo photo) {
        this.photos.add(photo);
    }

    public FinanceInfo getFinanceInfo() {
        return financeInfo;
    }

    public void setFinanceInfo(FinanceInfo financeInfo) {
        this.financeInfo = financeInfo;
    }
}

