package com.okan.springpostgresjsonb.domain;

public class FinanceInfo {

    private int commission;
    private int paymentTerm;

    public FinanceInfo(int commission, int paymentTerm) {
        this.commission = commission;
        this.paymentTerm = paymentTerm;
    }

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    public int getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(int paymentTerm) {
        this.paymentTerm = paymentTerm;
    }
}
