package com.okan.springpostgresjsonb.domain;

import java.util.Arrays;
import java.util.Optional;

public enum Color {

    WHITE(1, "WHITE"),
    BLACK(2, "BLACK"),
    RED(3, "RED"),
    GREEN(4, "GREEN"),
    BLUE(5, "BLUE"),
    PURPLE(6, "PURPLE"),
    YELLOW(7, "YELLOW"),
    PINK(8, "PINK"),
    GRAY(9, "GRAY"),
    ORANGE(10, "ORANGE");

    private int id;
    private String name;

    Color(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static Optional<Color> getColor(String name) {
        return Arrays.stream(Color.values()).filter(color -> color.name().equalsIgnoreCase(name)).findFirst();
    }



}
