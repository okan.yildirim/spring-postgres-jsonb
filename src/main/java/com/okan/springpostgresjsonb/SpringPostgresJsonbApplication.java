package com.okan.springpostgresjsonb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringPostgresJsonbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringPostgresJsonbApplication.class, args);
    }

}
