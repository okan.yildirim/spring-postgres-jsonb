package com.okan.springpostgresjsonb;

import com.github.javafaker.Faker;
import com.github.javafaker.Number;
import com.okan.springpostgresjsonb.domain.Color;
import com.okan.springpostgresjsonb.domain.FinanceInfo;
import com.okan.springpostgresjsonb.domain.Photo;
import com.okan.springpostgresjsonb.domain.Product;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.stream.IntStream;

@Service
public class ProductWriteService {

    private static final String PRODUCT_NAME_PREFIX = "PRODUCT_";
    private static final String PATH_PREFIX = "/photos/";

    private final ProductRepository productRepository;

    public ProductWriteService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @PostConstruct
    public void generateProduct() {

        IntStream.range(0, 10).forEach(
                j -> {
            Faker faker = new Faker();
            Number number = faker.number();
            faker.color();
            int barcode = number.numberBetween(1, 100000);
            double price = number.randomDouble(2, 1, 1000);

            int randomColorCount = number.numberBetween(1, 7);
            int randomSellerCount = number.numberBetween(0, 1000);
            int randomPhotoCount = number.numberBetween(0, 5);
            int randomCommission = number.numberBetween(0, 100);
            int randomPaymentTerm = number.numberBetween(0, 90);
            int randomWidth = number.numberBetween(0, 90);
            int randomHeight = number.numberBetween(0, 90);

            Product product = new Product();

            product.setName(PRODUCT_NAME_PREFIX + barcode);
            product.setPrice(price);

            IntStream.range(0, randomColorCount).forEach(i -> {
                String randomColor = faker.color().name();
                Color.getColor(randomColor).ifPresent(product::addColors);
            });

            IntStream.range(0, randomPhotoCount).forEach(i ->
                    product.addPhotos(new Photo(randomWidth, randomHeight, PATH_PREFIX + product.getName() + i)));

            IntStream.range(0, randomSellerCount).forEach(i ->
                    product.addSellerId(number.numberBetween(1L, 999999L))
            );

            product.setFinanceInfo(new FinanceInfo(randomCommission, randomPaymentTerm));

            productRepository.save(product);
                });
    }
}
