package com.okan.springpostgresjsonb;

import com.okan.springpostgresjsonb.domain.FinanceInfo;
import com.okan.springpostgresjsonb.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(value = "select * from products where seller_ids @> ?1 \\:\\:jsonb", nativeQuery = true)
    List<Product> findBySellerId(String sellerId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE products SET seller_ids = seller_ids || ?2\\:\\:jsonb WHERE id = ?1", nativeQuery = true)
    int addSellerIdToProduct(Long id, String sellerId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE products SET seller_ids = seller_ids - ?2 WHERE id = ?1", nativeQuery = true)
    int removeSellerIdFromProduct(Long id, Long sellerId); // TODO


    @Query(value = "select * from products where colors @> CAST(:color AS jsonb)", nativeQuery = true)
    List<Product> findByColor(@Param("color") String color);

    @Modifying
    @Transactional
    @Query(value = "UPDATE products SET colors = colors || ?2\\:\\:jsonb WHERE id = ?1", nativeQuery = true)
    int addColorToProduct(Long id, String color);

    @Modifying
    @Transactional
    @Query(value = "UPDATE products SET colors = colors - ?2 WHERE id = ?1", nativeQuery = true)
    int removeColorFromProduct(Long id, String color);


    List<Product> findByFinanceInfo(FinanceInfo financeInfo);

    @Query(value = "select * from products where cast(finance_info ->> 'commission' as int) > :commission", nativeQuery = true)
    List<Product> findCommissionGreaterThan(@Param("commission") Double commission);

    @Modifying
    @Transactional
    @Query(value = "UPDATE products SET finance_info = jsonb_set(finance_info, '{paymentTerm}', ?2\\:\\:jsonb) WHERE id = ?1", nativeQuery = true)
    int updatePaymentTerm(Long id, String paymentTerm);

}
